<?php
namespace App\Http\Controllers;


use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
    return $request->user();
});


Route::group([ 'prefix' => 'auth' ], function () {
    Route::post('login', [AuthApiController::class, 'login']);
    Route::post('signup', [AuthApiController::class, 'signUp']);
});



Route::group(['middleware' => ['cors']], function () {
    Route::get('users', [UserController::class, 'index']);
});

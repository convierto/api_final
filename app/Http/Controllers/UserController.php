<?php

namespace App\Http\Controllers;

use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Http\JsonResponse;

class UserController extends Controller
{
    public function index() :JsonResponse
    {
        $users = User::all();

        return response()->json([
            'data' => $users,
            'message' => 'Usuarios Obtenidos con exito!',
            'status' => 200
        ]);
    }
}

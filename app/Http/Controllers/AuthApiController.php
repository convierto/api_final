<?php

namespace App\Http\Controllers;

use App\Models\{User, Paquetes};
use Illuminate\Http\Request;
use Illuminate\Support\Facades\{Auth, Hash};
use Carbon\Carbon;
use Illuminate\Support\Str;

class AuthApiController extends Controller
{
    public function login(Request $request)
    {
        $credentials = request(['email', 'password']);
        if (!Auth::attempt($credentials))
            return response()->json([
                'message' => 'Unauthorized'
            ], 401);

        $user = $request->user();

        return response()->json([
            'user' => $user,
            'access_token' => 'token',
            'token_type' => 'Bearer',
            'expires_at' => 'expired'
        ]);
    }

    public function signUp(Request $request)
    {
        $user = User::create([
            'uuid' => Str::uuid(),
            'name' => $request->name,
            'lastName' => $request->lastName,
            'email' => $request->email,
            'password' => Hash::make($request->password),
        ]);

        $paquete_free = Paquetes::where('proof', 1)->first();

        if($paquete_free != null)
        {
            $date = Carbon::parse(date('Y-m-d'));
            $endDate = $date->addDay($paquete_free->daysDuration);
            $membresia = Memebresia::create([
                'uuid' => Str::uuid(),
                'idPackage' => $paquete_free->id,
                'idUser' => $user->id,
                'start' => date('Y-m-d'),
                'end' => $endDate,
                'idPay' => 0
            ]);

            $user->idMembresia = $membresia->id;
            $user->save();
        }

        return response()->json([
            'message' => 'Successfully created user!'
        ], 201);
    }
}
